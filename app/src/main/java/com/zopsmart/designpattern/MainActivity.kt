package com.zopsmart.designpattern

import android.os.Bundle
import com.zopsmart.designpattern.databinding.ActivityMainBinding
import com.zopsmart.designpattern.util.ViewBindingActivity

class MainActivity : ViewBindingActivity<ActivityMainBinding>(
    ActivityMainBinding::inflate
) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
